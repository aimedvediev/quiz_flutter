import 'package:flutter/material.dart';

import "quiz.dart";
import "result.dart";

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': "What's yor favorite color?",
      "answers": [
        {'text': 'Black', 'score': 1},
        {'text': 'White', 'score': 2},
        {'text': 'Red', 'score': 3},
        {'text': 'Blue', 'score': 4},
        {'text': 'Green', 'score': 5},
      ],
    },
    {
      'questionText': "What's yor favorite animal?",
      "answers": [
        {'text': 'Deer', 'score': 1},
        {'text': 'Wolf', 'score': 2},
        {'text': 'Dog', 'score': 3},
        {'text': 'Cat', 'score': 4},
        {'text': 'Bear', 'score': 5},
      ],
    },
  ];

  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestions(int score) {
    if (_questionIndex < _questions.length) {}
    _totalScore = _totalScore + score;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("MyFirstApp"),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestions,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
